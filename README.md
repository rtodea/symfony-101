# README #

We are following the [Symfony tutorial](http://symfony.com/doc/current/quick_tour/the_big_picture.html).

### Setup ###
We are using LXC for virtualisation with the repository exposed.

E.g.

`sudo lxc-create -n symfony -t ubuntu`

`symfony` is in `/var/lib/lxc/symfony/`

`fstab` is in `/var/lib/lxc/symfony/fstab`

```
#!
/home/anima/repo home/ubuntu/repo none bind,create=dir
```