#!/bin/bash -e
#
#  Install Symfony


update_php_ini() {
  local php_ini=$(\
    php -i \
    | grep "Loaded Configuration File" \
    | cut -d">" -f2 \
  )

  sed -i "s/^;date.timezone =$/date.timezone = \"Europe\/Bucharest\"/" $php_ini
}


install_symfony() {
  curl -LsS http://symfony.com/installer > symfony.phar
  mv symfony.phar /usr/local/bin/symfony
  chmod a+x /usr/local/bin/symfony
}


main() {
  update_php_ini
  install_symfony
}


main "$@"
