#!/bin/bash -e
#
# Provision the LXC container for Symfony


fix_locale() {
  locale-gen ro_RO.UTF-8 en_US.UTF-8 en_US
  dpkg-reconfigure locales 
}


install_general_utilities() {
  apt-get update
  apt-get dist-upgrade -y
  apt-get install -y \
    htop \
    curl \
    locate \
    tree \
    lynx \
    acl

  updatedb
}


install_php_apache() {
  apt-get install -y \
    php5 \
    apache2

  # fix ServerName warning
  echo "ServerName localhost" >> /etc/apache2/apache2.conf
}

# timezone
setup_timezone() {
  apt-get install -y \
    php5-intl
}


main() {
  fix_locale
  install_general_utilities
  install_php_apache
  setup_timezone
}


main "$@"
