Adapt MOD 1-4 - local vagrant
===

### Clone vagrant into your *dev* directory

	mkdir <dev directory>/vagrant
	cd <dev directory>/vagrant
	git clone git@github.com:e-spres-oh/adapt-vagrant.git .

### Clone MOD 1-4 into your *dev* directory

	mkdir <dev directory>/adapt
	cd <dev directory>/adapt

	git clone git@bitbucket.org:adaptcourseware/core.git --recursive
	git clone git@bitbucket.org:adaptcourseware/mod1.git
	git clone git@bitbucket.org:adaptcourseware/mod2.git
	git clone git@bitbucket.org:adaptcourseware/mod3.git
	git clone git@bitbucket.org:adaptcourseware/mod4.git --recursive

### Install vagrant and bootstrap env

Install **virtualbox**

	https://www.virtualbox.org/

Install **vagrant**

	http://www.vagrantup.com/

Create and provision virtual env

	cd <dev directory>/vagrant
	vagrant up

Setup ssh key in vagrant

	vagrant ssh
	nano ~/.ssh/id_rsa
	paste the contents of your ssh key (the one that has access to Adapt Bitbucket)
	save the file *ctrl+O*

### Add local domain to hosts file on our mac

	echo '3.5.0.10 	core.local' | sudo tee -a /etc/hosts
	echo '3.5.0.10 	mod1.local' | sudo tee -a /etc/hosts
	echo '3.5.0.10 	mod2.local' | sudo tee -a /etc/hosts
	echo '3.5.0.10 	mod3.local' | sudo tee -a /etc/hosts
	echo '3.5.0.10 	mod4.local' | sudo tee -a /etc/hosts

On Windows the usual location for the hosts file is

	C:\Windows\System32\Drivers\etc\hosts

### Configure modules

	vagrant ssh

### Add OAuth token for Github

https://help.github.com/articles/creating-an-access-token-for-command-line-use/

	composer config -g github-oauth.github.com <oauthtoken>

#### Core

	cd /var/www/adapt/core
	git checkout develop

	!!! IMPORT core database into `core`

	cd /var/www/adapt/core/v2
	composer install

#### MOD1

	cd /var/www/adapt/mod1
	git checkout develop
	composer install

#### MOD2

	cd /var/www/adapt/mod2
	git checkout develop
	composer install
	./app/console fos:js-routing:dump
	./app/console assetic:dump --env=dev
	./app/console assetic:dump --env=prod
	./app/console doctrine:migrations:migrate --no-interaction

	ONLY ON WINDOWS!!!
	./app/console assets:install ./web/

Add hosts in paramenters.local.ini

	acs_core	= 'http://devcore.h4cloud.com'
	acs_mod2	= 'https://mod2.local'
	acs_mod3	= 'http://devm3.h4cloud.com'

#### MOD3

	cd /var/www/adapt/mod3
	git checkout develop
	composer install
	./app/console assetic:dump --env=dev
	./app/console assetic:dump --env=prod
	./app/console doctrine:migrations:migrate --no-interaction

	ONLY ON WINDOWS!!!
	./app/console assets:install ./web/

#### MOD4

	cd /var/www/adapt/mod4
	git checkout develop
	composer install
	./app/console fos:js-routing:dump
	./app/console assetic:dump --env=dev
	./app/console assetic:dump --env=prod
	./app/console doctrine:schema:update --no-ansi --force
