#!/bin/bash -e
#
# Install NodeJS using NVM: https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server

setup_pre() {
  apt-get install -y \
    curl \
    python \
    build-essential \
    libssl-dev
}


install_nvm() {
  curl https://raw.githubusercontent.com/creationix/nvm/v0.16.1/install.sh | sh
  source ~/.profile
}

install_latest() {
  local ver=$(\
    nvm ls-remote \
    | tail -n 1 \
    | tr -d [[:space:]] \
    | cut -c 2-
  )
  nvm install $ver
}


main() {
  setup_pre
  install_nvm
}


main "$@"

