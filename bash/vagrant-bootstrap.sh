#!/bin/bash
(
	export DEBIAN_FRONTEND=noninteractive

	touch /home/vagrant/.nano_history
	chown vagrant:vagrant /home/vagrant/.nano_history

	locale-gen UTF-8

	echo ""
	echo "### Fixing dns entries"
	sed -i -e"s/domain-name-servers, //g" /etc/dhcp/dhclient.conf
	if [ -z "`grep -Fl 'prepend domain-name-servers 8.8.8.8,8.8.4.4;' /etc/dhcp/dhclient.conf`" ]; then
		echo $'\n'"prepend domain-name-servers 8.8.8.8,8.8.4.4;" >> /etc/dhcp/dhclient.conf
	fi
	(dhclient -r && dhclient eth0)

	echo ""
	echo "### Add color prompt"
	touch /home/vagrant/.nano_history
	chown vagrant:vagrant /home/vagrant/.nano_history
	sed -i -e"s/#force_color_prompt=yes/force_color_prompt=yes/g" /home/vagrant/.bashrc
	source /home/vagrant/.bashrc

	echo ""
	echo "### Updating apt data"
	apt-get update

	echo "### Installing necessary packages"
	apt-get -q -y install \
		build-essential htop git openssl ssl-cert mysql-server-5.6 \
		apache2 nodejs-legacy npm php5 \
		php5-mysqlnd php5-curl php5-json php5-mcrypt php5-intl php5-xdebug \
		php-pear php5-dev php5-geoip libgeoip-dev php5-gd

	pecl install geoip

	gem install compass

	npm install --silent -g uglifyjs
	npm install --silent -g uglifycss
	npm install --silent -g less@v1.7.5

	npm install --silent -g compass
	npm install --silent -g bower
	npm install --silent -g gulp

	echo ""
	echo "### Configure MySQL for remote connections"
	sed -i -e"s/127.0.0.1/0.0.0.0/g" /etc/mysql/my.cnf
	mysql -uroot -e"GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;"
	# Configure MySQl strict mode
	MYSQL_SETTINGS='[mysqld]
sql_mode=NO_ENGINE_SUBSTITUTION,STRICT_TRANS_TABLES'
	echo "${MYSQL_SETTINGS}" > /etc/mysql/conf.d/90-adapt.ini

	echo "### PHP settings"
	PHP_SETTINGS='display_errors = On
error_reporting = E_ALL
date.timezone = America/New_York
session.auto_start = 0

[PHP]
geoip.custom_directory=/usr/share/GeoIP'
	echo "${PHP_SETTINGS}" > /etc/php5/apache2/conf.d/90-adapt.ini

	cp /etc/php5/apache2/conf.d/90-adapt.ini /etc/php5/cli/conf.d/

	# enable mcrypt mod
	php5enmod mcrypt
	php5dismod xdebug

	echo ""
	echo "### Configuring Apache Vhost"
	VHOST='<VirtualHost *:80>
    DocumentRoot "/var/www/adapt/core"
    ServerName core.local

    <Directory "/var/www/adapt/core">
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
    </Directory>

    Alias /v2/ /var/www/adapt/core/v2/web/
</VirtualHost>'
	echo "${VHOST}" > /etc/apache2/sites-available/core.conf

	VHOST='ServerName mod1.local
<VirtualHost *:80>
    DocumentRoot "/var/www/adapt/mod1"
    ServerName mod1.local

    <Directory "/var/www/adapt/mod1">
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
    </Directory>
</VirtualHost>'
	echo "${VHOST}" > /etc/apache2/sites-available/mod1.conf

	VHOST='<VirtualHost *:443>
	DocumentRoot "/var/www/adapt/mod2/web"
	ServerName mod2.local

	SSLEngine on
	SSLCertificateFile "/etc/apache2/ssl/adapt.crt"
	SSLCertificateKeyFile "/etc/apache2/ssl/adapt.key"

	<Directory "/var/www/adapt/mod2/web">
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>'
	echo "${VHOST}" > /etc/apache2/sites-available/mod2.conf

	VHOST='<VirtualHost *:80>
	DocumentRoot "/var/www/adapt/mod3/web"
	ServerName mod3.local

	<Directory "/var/www/adapt/mod3/web">
		Options Indexes FollowSymLinks
		AllowOverride All
		Require all granted
	</Directory>
</VirtualHost>'
	echo "${VHOST}" > /etc/apache2/sites-available/mod3.conf

	VHOST='<VirtualHost *:80>
	DocumentRoot "/var/www/adapt/mod4/web"
	ServerName mod4.local

	<Directory "/var/www/adapt/mod4/web">
		Options Indexes FollowSymLinks
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>
</VirtualHost>'
	echo "${VHOST}" > /etc/apache2/sites-available/mod4.conf

	a2enmod rewrite
	a2enmod ssl

	a2dissite 000-default
	a2ensite core
	a2ensite mod1
	a2ensite mod2
	a2ensite mod3
	a2ensite mod4

	echo ""
	echo "### Configure Apache user"
    sed -i -e"s/export APACHE_RUN_USER=www-data/export APACHE_RUN_USER=vagrant/g" /etc/apache2/envvars
    sed -i -e"s/export APACHE_RUN_GROUP=www-data/export APACHE_RUN_GROUP=vagrant/g" /etc/apache2/envvars

    echo ""
    echo "### Copy apache SSL keys"
    mkdir -p /etc/apache2/ssl
    cp /vagrant/ssl_keys/* /etc/apache2/ssl

	service apache2 restart

	# install composer globaly
	curl -sS https://getcomposer.org/installer | php
	mv composer.phar /usr/local/bin/composer

	# install phpuniy globaly
	wget -q https://phar.phpunit.de/phpunit.phar
	chmod +x phpunit.phar
	mv phpunit.phar /usr/local/bin/phpunit

	echo "" >> /home/vagrant/.bashrc
	echo "export PATH=/home/vagrant/.composer/vendor/bin:/var/www/current/core/bin:$PATH" >> /home/vagrant/.bashrc
	source /home/vagrant/.bashrc

	echo ""
	echo "Do some aptitude cleanup"
	apt-get -q -y autoremove

	echo ""
	echo "Configure GeoIP"
	mkdir -p /usr/share/GeoIP
	cd /usr/share/GeoIP
	wget -q http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
	gunzip GeoLiteCity.dat.gz
	mv GeoLiteCity.dat GeoIPCity.dat

	service apache2 restart

	echo ""
	echo "### Bootstrap completed"

	echo ""
	echo "### Configure CORE"
	cd /var/www/adapt/core
	mysql -uroot -e"CREATE DATABASE core;"
	if [ ! -f ./config_local.php ]; then
		cp ./config.php ./config_local.php
		sed -i -e"s/'host'     => 'dev1db-1-int.h4cloud.com'/'host'     => 'localhost'/g" ./config_local.php
		sed -i -e"s/'password' => 'headware4'/'password' => ''/g" ./config_local.php
		sed -i -e"s/'database' => 'core_beta'/'database' => 'core'/g" ./config_local.php

		echo "
PATH=$PATH:/var/www/adapt/core/libraries/lithium/console
" >> /home/vagrant/.bashrc
	fi

	mkdir -p application/resources/tmp/cache/templates

	if [ ! -f ./v2/app/config/parameters.yml ]; then
		cp ./v2/app/config/parameters.yml.dist ./v2/app/config/parameters.yml
		sed -i -e"s/database_name:     symfony/database_name:     core/g" ./v2/app/config/parameters.yml
		sed -i -e"s/database_name_test: symfony_test/database_name_test: core_test/g" ./v2/app/config/parameters.yml
	fi

	echo ""
	echo "### Configure MOD1"
	cd /var/www/adapt/mod1
	mysql -uroot -e"CREATE DATABASE module_1;"
	mysql -uroot -B module_1 < ac_mod1.sql
	if [ ! -f ./api/_config_local.php ]; then
		cp ./api/_config.php ./api/_config_local.php
		sed -i -e"s/define('DB_HOST', 'devdb1.h4cloud.com');/define('DB_HOST', 'localhost');/g" ./api/_config_local.php
		sed -i -e"s/define('DB_HOST', 'devdb1.h4cloud.com');/define('DB_HOST', 'localhost');/g" ./api/_config_local.php
		sed -i -e"s/define('DB_PASSWORD', 'headware4');/define('DB_PASSWORD', '');/g" ./api/_config_local.php
		sed -i -e"s/define('DB_NAME', 'mod1_rework');/define('DB_NAME', 'module_1');/g" ./api/_config_local.php
	fi
	mkdir -p ./api/uploads

	echo ""
	echo "### Configure MOD2"
	cd /var/www/adapt/mod2
	mysql -uroot -e"CREATE DATABASE module_2;"
	ln -s /usr/bin/node /usr/local/bin/node
	ln -s /usr/local/lib/node_modules /usr/lib/node_modules
	ln -s /usr/local/bin/uglifyjs /usr/bin/uglifyjs
	ln -s /usr/local/bin/uglifycss /usr/bin/uglifycss

	if [ ! -f ./app/config/parameters.local.ini ]; then
		cp ./app/config/parameters.local.ini.dist ./app/config/parameters.local.ini
		sed -i -e's/database_host     = ""/database_host     = "localhost"/g' ./app/config/parameters.local.ini
		sed -i -e's/database_name     = ""/database_name     = "module_2"/g' ./app/config/parameters.local.ini
		sed -i -e's/database_user     = ""/database_user     = "root"/g' ./app/config/parameters.local.ini
		sed -i -e's/coredb_host       = ""/coredb_host       = "devdb1.h4cloud.com"/g' ./app/config/parameters.local.ini
		sed -i -e's/coredb_name       = ""/coredb_name       = "core_beta"/g' ./app/config/parameters.local.ini
		sed -i -e's/coredb_user       = ""/coredb_user       = "root"/g' ./app/config/parameters.local.ini
		sed -i -e's/coredb_password   = ""/coredb_password   = "headware4"/g' ./app/config/parameters.local.ini
		rm -rf app/cache/* app/logs/* web/css/* web/js/* web/bundles/* vendor/* src/Esoh/AcsCommon
	fi

	echo ""
	echo "### Configure MOD3"
	cd /var/www/adapt/mod3
	mysql -uroot -e"CREATE DATABASE module_3;"
	if [ ! -f ./app/config/parameters.local.yml ]; then
		cp ./app/config/parameters.sample.yml ./app/config/parameters.yml
		cp ./app/config/parameters.local.sample.yml ./app/config/parameters.local.yml
		sed -i -e's/database_name: mod3/database_name: module_3/g' ./app/config/parameters.local.yml
		sed -i -e's/database_password: headware4/database_password: ""/g' ./app/config/parameters.local.yml
		sed -i -e's/database_port: null/database_port: "3306"/g' ./app/config/parameters.local.yml
		rm -rf app/cache/* app/logs/* web/css/* web/js/* vendor/*
	fi

	echo ""
	echo "### Configure MOD4"
	cd /var/www/adapt/mod4
	mysql -uroot -e"CREATE DATABASE module_4;"
	if [ ! -f ./app/config/parameters.local.yml ]; then
		cp ./app/config/parameters.local.yml.dist ./app/config/parameters.local.yml
		sed -i -e's/database_password: headware4/database_password: ""/g' ./app/config/parameters.local.yml
		sed -i -e's/#    acs_url_core/    acs_url_core/g' ./app/config/parameters.local.yml
		rm -rf app/cache/* app/logs/* web/css/* web/js/* web/bundles/* vendor/*
	fi


	echo ""
	echo "### Done... Follow the steps in README.md now"
)
#2>&1 | logger -t vagrant.bootstrap
exit 0
